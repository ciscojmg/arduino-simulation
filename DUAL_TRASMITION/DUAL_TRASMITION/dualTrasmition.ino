unsigned char counter = 0;
unsigned long timeStart = 0;
const unsigned char REQUEST = 3;
const unsigned char STATUS_TRANSMIT = 2;

void setup(){
  Serial.begin(9600);
  pinMode(STATUS_TRANSMIT, OUTPUT);
  pinMode(REQUEST, INPUT);
}

void loop(){
  task();
}


void task(){
  timeStart = millis();
  taskOne(1000);
  return;
}

void taskOne(unsigned long samplingTimeMillis){
  static unsigned long timeTaskOne = 0;

  if (timeTaskOne == 0)
    timeTaskOne = millis();

  if ( timeStart >= timeTaskOne + samplingTimeMillis ) {
    while ( digitalRead(REQUEST) == 0 ) {

        digitalWrite(STATUS_TRANSMIT, HIGH);
        delay(20);
        Serial.print("MCU Master: ");
        Serial.println(counter);
        delay(20);
        counter++;

        digitalWrite(STATUS_TRANSMIT, LOW);
        break;
    }
    timeTaskOne = millis();
  }

  return;
}
