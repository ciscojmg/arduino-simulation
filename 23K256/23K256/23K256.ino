// Basic Arduino / 23K256 Srial RAM communication demo
// by Ido Gendel, 2012
// Share and enjoy!

// The HOLD pin of the chip can be connected directly to Vcc
// Remember Vcc must be at most 4.5V
int RAMClockPin = 2;
int RAMInPin = 3;
int RAMOutPin = 4; 
int RAMSelectPin = 5;

// These are hard-wired in the chip
int RAM_READ_COMMAND = 3;    // 00000011
int RAM_WRITE_COMMAND = 2; // 00000010 

//================================================

// Send a byte into the chip's input, bit by bit
void RAMRawWrite(byte b) {
  
  // Cycle through the 8 bits in the byte
  for (int j = 0; j < 8; j++) {
     
     // Send MSB (Most Significant Bit) to chip
     if ((b & 0x80) == 0x80) digitalWrite(RAMInPin, HIGH);
       else digitalWrite(RAMInPin, LOW);
     // Send a clock pulse
     digitalWrite(RAMClockPin, HIGH); 
     digitalWrite(RAMClockPin, LOW); 
     // Shift the rest of the bits, one to the left
     b = b << 1;
     
   }  
  
}

//================================================

// The whole procedure of sending a data byte
void writeToRAM(unsigned int wAddr, byte wValue) {
  
  // Pointer to a byte
  byte *bp;
  
  // Point to higher byte of address (wAddr is 2 bytes long)
  bp = (byte *)&wAddr + 1;
  // Start session
  digitalWrite(RAMSelectPin, LOW);
  // Send the "write" command
  RAMRawWrite(RAM_WRITE_COMMAND);
  // Send the address, one byte at a time
  RAMRawWrite(*bp); // Higher byte
  bp--;
  RAMRawWrite(*bp); // Lower byte
  // Send the data byte
  RAMRawWrite(wValue);
  // Close session
  digitalWrite(RAMSelectPin, HIGH);   
  
}

//================================================

// The whole procedure of reading a data byte
byte readFromRAM(unsigned int rAddr) {
  
  byte b, currBit;
  byte *bp;
   
  // Point to higher byte of address
  bp = (byte *)&rAddr + 1;
  // Start session
  digitalWrite(RAMSelectPin, LOW);
  // Send the "read" command
  RAMRawWrite(RAM_READ_COMMAND);
  // Send address
  RAMRawWrite(*bp); // Higher byte
  bp--;
  RAMRawWrite(*bp); // Lower byte

  // Get bits, MSB-first
  b = 0;
  for (byte currBit = 0x80; currBit > 0; currBit = currBit >> 1) {
     
     // Clock signal start
     digitalWrite(RAMClockPin, HIGH); 
     // Get one bit of data, put it in the appropriate place 
     if (digitalRead(RAMOutPin) == HIGH) b += currBit;
     // Clock signal end
     digitalWrite(RAMClockPin, LOW); 
     
  }  

  // End session and return result
  digitalWrite(RAMSelectPin, HIGH);   
  return b;
  
}

//================================================

void setup() {
  Serial.begin(9600);
  pinMode(RAMClockPin, OUTPUT);
  pinMode(RAMInPin, OUTPUT);
  pinMode(RAMOutPin, INPUT);
  pinMode(RAMSelectPin, OUTPUT);  
  digitalWrite(RAMSelectPin, HIGH);
}

void loop() {

  for (unsigned char i = 0; i < 100; i++) {
    writeToRAM(i, i);
    delay(10);
  }

  delay(500);

  for (unsigned char i = 0; i < 100; i++) {
    Serial.print("address: ");
    Serial.print(i, HEX);
    Serial.print(" value: ");
    Serial.println(readFromRAM(i), HEX);
    delay(50);
  }
  
  while (1);
  
}